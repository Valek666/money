﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Money.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public string SenderId { get; set; }
        public ApplicationUser SenderUser { get; set; }
        public int SenderBalance { get; set; }

        public string RecientId { get; set; }
        public ApplicationUser RecientUser { get; set; }
        public int RecientBalance { get; set; }

        public int Sum { get; set; }
        public DateTime Date { get; set; }


    }
}
